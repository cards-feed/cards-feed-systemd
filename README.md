# Репозитогий с конфигурацией systemd

## Конспект для меня будущего

Чтоб это заработало, не конфликтовало с другими сервисами и было безопасно для системы, я создал пользователя

```
sudo adduser --system --group --home /var/lib/cards-feed-gitlab-runner cards-feed-gitlab-runner
```

добавил в файл /etc/sudoers.d/cards-feed-gitlab-runner

```
User_Alias USERNAME = cards-feed-gitlab-runner

# Allow to create services
USERNAME ALL=(root) NOPASSWD: /usr/bin/cp * /etc/systemd/system/cards-feed[a-zA-Z0-9-]*.service

# Allow to restart services
USERNAME ALL=(root) NOPASSWD: /usr/bin/systemctl daemon-reload
USERNAME ALL=(root) NOPASSWD: /usr/bin/systemctl restart cards-feed[a-zA-Z0-0-]*.service
```

зарегистрировал гитлаб раннера
```
sudo gitlab-runner register \
    --url https://gitlab.com \
    --token <token> \
    --config /etc/gitlab-runner/cards-feed-gitlab-runner.toml
```

и выполнил

```
sudo gitlab-runner install \
    --config /etc/gitlab-runner/cards-feed-gitlab-runner.toml \
    --working-directory /var/lib/cards-feed-gitlab-runner \
    --service cards-feed-gitlab-runner \
    --user cards-feed-gitlab-runner

sudo gitlab-runner start --service cards-feed-gitlab-runner
```
